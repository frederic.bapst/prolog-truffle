package ch.heiafr.prolograal.runtime;

import java.util.Map;

/**
 * Class representing a Prolog atom.
 * @author Martin Spoto
 */
public final class ProloGraalAtom extends ProloGraalTerm<ProloGraalAtom> {
   // the name of the atom
   private final String name;

   public ProloGraalAtom(Map<ProloGraalVariable, ProloGraalVariable> variables, String name) {
      super(variables);
      this.name = name;
      this.hashCode = name.hashCode();
   }

   @Override
   public String toString() {
      return name;
   }

   public String getName() {
      return name;
   }

   @Override
   public boolean equals(Object obj) {
      // two atoms are equal if they have the same name
      return obj instanceof ProloGraalAtom && this.name.equals(((ProloGraalAtom) obj).name);
   }

   /**
    * Unify this atom with the given term.<br>
    * An atom is unifiable to another atom having the same name or to a variable, given that the variable is either
    * currently unbound or bound to an unifiable atom.
    * @return True if this atom is unifiable with the other term
    */
   @Override
   public boolean unify(ProloGraalTerm<?> other) {
      if (this.equals(other)) {
         return true;
      } else if (other instanceof ProloGraalVariable) {
         // if other is a variable we delegate the unification process to the variable
         return other.unify(this);
      }
      return false;
   }

   @Override
   public ProloGraalAtom copy(Map<ProloGraalVariable, ProloGraalVariable> variables) {
      return new ProloGraalAtom(variables, this.name);
   }
}