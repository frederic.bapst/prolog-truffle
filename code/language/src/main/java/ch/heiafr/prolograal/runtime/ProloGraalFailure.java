package ch.heiafr.prolograal.runtime;

/**
 * Class representing a failure during the resolution process.
 */
public class ProloGraalFailure extends ProloGraalBoolean {
   @Override
   public String toString() {
      return "no";
   }

   @Override
   public boolean asBoolean() {
      return false;
   }
}