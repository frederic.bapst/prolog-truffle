package ch.heiafr.prolograal.runtime;

import java.util.Map;

/**
 * Class representing a Prolog integer number. Unification is handled in the super class.
 * @author Martin Spoto
 */
public final class ProloGraalIntegerNumber extends ProloGraalNumber<ProloGraalIntegerNumber> {
   private final int value;

   public ProloGraalIntegerNumber(Map<ProloGraalVariable, ProloGraalVariable> variables, int value) {
      super(variables);
      this.value = value;
      this.hashCode = Integer.hashCode(value);
   }

   @Override
   public double asDouble() {
      return (double) value;
   }

   @Override
   public int asInt() {
      return value;
   }

   @Override
   public String toString() {
      return value + "";
   }

   @Override
   public boolean equals(Object obj) {
      return obj instanceof ProloGraalIntegerNumber && this.value == ((ProloGraalIntegerNumber) obj).value;
   }

   @Override
   public ProloGraalIntegerNumber copy(Map<ProloGraalVariable, ProloGraalVariable> variables) {
      return new ProloGraalIntegerNumber(variables, this.value);
   }
}