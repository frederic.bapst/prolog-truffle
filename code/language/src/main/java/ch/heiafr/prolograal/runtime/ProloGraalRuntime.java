package ch.heiafr.prolograal.runtime;

import ch.heiafr.prolograal.builtins.predicates.ProloGraalBuiltinClause;
import ch.heiafr.prolograal.builtins.predicates.ProloGraalVarBuiltin;
import ch.heiafr.prolograal.builtins.predicates.ProloGraalWriteBuiltin;

import java.util.*;

/**
 * Class holding a runtime. A runtime is composed of a number of clauses, and of the context. <br>
 * Built-in predicates are also added by this class using the {@link #installBuiltins()} method.
 * @see ProloGraalBuiltinClause
 * @author Martin Spoto
 */
public final class ProloGraalRuntime {
   // the clauses. A map is used to allow fast filtering using the head of a clause.
   // each map entry contains the list of clauses sharing the same head.
   private final Map<ProloGraalTerm<?>, List<ProloGraalClause>> clauses;
   // context reference, in case we need to pass it to builtins.
   private final ProloGraalContext context;

   // reference to the first clause for fast retrieval
   private ProloGraalClause firstClause;

   /**
    * Creates a new runtime containing the clauses from the given list.
    * @param clauseList The list of clauses that should be in this runtime.
    * @param context The context of this runtime
    */
   public ProloGraalRuntime(List<ProloGraalClause> clauseList, ProloGraalContext context) {
      this.context = context;
      clauses = new HashMap<>();

      if (clauseList.size() > 0) {
         firstClause = clauseList.get(0);
      }

      // put every clauses into the map
      for (ProloGraalClause clause : clauseList) {
         // create a new list if there is none for the current clause's head
         clauses.putIfAbsent(clause.getHead(), new ArrayList<>());
         // retrieve the list linked to the head of the current clause
         List<ProloGraalClause> clauses1 = clauses.get(clause.getHead());
         // add this clause into the map
         clauses1.add(clause);
      }

      installBuiltins();
   }

   /**
    * Install built-ins predicate. Every new built-in should be added here.
    */
   private void installBuiltins() {
      ProloGraalClause varBuiltin = new ProloGraalVarBuiltin();
      clauses.put(varBuiltin.getHead(), Collections.singletonList(varBuiltin));

      ProloGraalClause writeBuiltin = new ProloGraalWriteBuiltin(context);
      clauses.put(writeBuiltin.getHead(), Collections.singletonList(writeBuiltin));
   }

   public final Map<ProloGraalTerm<?>, List<ProloGraalClause>> getClauses() {
      return clauses;
   }

   public final ProloGraalClause getFirstClause() {
      if (firstClause == null) {
         throw new IllegalStateException("Runtime is empty !");
      }
      return firstClause;

   }
}