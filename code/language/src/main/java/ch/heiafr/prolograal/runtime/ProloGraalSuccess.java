package ch.heiafr.prolograal.runtime;

import ch.heiafr.prolograal.nodes.ProloGraalProofTreeNode;
import ch.heiafr.prolograal.nodes.ProloGraalResolverNode;

import java.util.Map;

/**
 * Class representing a success.
 * It is capable of holding the state of the variables the lead to the success.
 * @see ProloGraalResolverNode
 * @see ProloGraalProofTreeNode
 * @author Martin Spoto
 */
public class ProloGraalSuccess extends ProloGraalBoolean {

   private Map<ProloGraalVariable, ProloGraalVariable> variables;

   public ProloGraalSuccess() {

   }

   public ProloGraalSuccess(Map<ProloGraalVariable, ProloGraalVariable> variables) {
      this.variables = variables;
   }

   public Map<ProloGraalVariable, ProloGraalVariable> getVariables() {
      return this.variables;
   }

   @Override
   public String toString() {
      return "yes";
   }

   @Override
   public boolean asBoolean() {
      return true;
   }
}