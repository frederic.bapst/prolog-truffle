package ch.heiafr.prolograal.nodes;

import ch.heiafr.prolograal.ProloGraalLanguage;
import ch.heiafr.prolograal.runtime.ProloGraalContext;
import ch.heiafr.prolograal.runtime.ProloGraalRuntime;
import com.oracle.truffle.api.CompilerDirectives;
import com.oracle.truffle.api.CompilerDirectives.CompilationFinal;
import com.oracle.truffle.api.Truffle;
import com.oracle.truffle.api.TruffleLanguage.ContextReference;
import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.nodes.RootNode;

/**
 * Class representing the Truffle root node for evaluation of source code.
 * Taken and adapted from the SimpleLanguage demo implementation.
 * @see ProloGraalLanguage
 * @author Martin Spoto
 */
public final class ProloGraalEvalRootNode extends RootNode {

   private final ProloGraalLanguage language;
   private final ProloGraalRuntime runtime;
   @CompilationFinal
   private boolean registered;

   private final ContextReference<ProloGraalContext> reference;

   public ProloGraalEvalRootNode(ProloGraalLanguage language, ProloGraalRuntime runtime) {
      super(null); // internal frame
      this.runtime = runtime;
      this.language = language;
      this.reference = language.getContextReference();
   }

   @Override
   public String getName() {
      return "ProloGraalEvalRootNode";
   }

   @Override
   public String toString() {
      return getName();
   }

   @Override
   public Object execute(VirtualFrame frame) {
      /* Lazy registrations of clauses on first execution. */
      if (!registered) {
         /* Function registration is a slow-path operation that must not be compiled. */
         CompilerDirectives.transferToInterpreterAndInvalidate();
         reference.get().registerRuntime(runtime);

         ProloGraalInterpreterNode interpreterNode = new ProloGraalInterpreterNode(language);
         ProloGraalResolverNode resolverNode = new ProloGraalResolverNode(language);

         reference.get().registerInterpreter(interpreterNode);
         reference.get().registerResolver(resolverNode);

         registered = true;
      }

      return Truffle.getRuntime().createCallTarget(reference.get().getInterpreterNode()).call();
   }
}
