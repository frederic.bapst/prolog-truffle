package ch.heiafr.prolograal.builtins.predicates;

import ch.heiafr.prolograal.runtime.ProloGraalClause;
import ch.heiafr.prolograal.runtime.ProloGraalRuntime;

/**
 * Abstract base class for built-ins.
 * A built-in must inherit from this class and be added to the built-in list to work.
 * The built-in list is handled in the installBuiltins method from {@link ProloGraalRuntime}.
 * @see ProloGraalRuntime
 * @author Martin Spoto
 */
public abstract class ProloGraalBuiltinClause extends ProloGraalClause {
   /**
    * Overridable execute method for built-ins.
    * May have side effects, like writing or opening a file.
    */
   public void execute() {
      // default behaviour is to do nothing...
   }

   /**
    * We want the built-ins to override this because otherwise they will lose their "ProloGraalBultinClause" status.
    * @return a copy of the built-in
    */
   @Override
   public abstract ProloGraalClause copy();
}
