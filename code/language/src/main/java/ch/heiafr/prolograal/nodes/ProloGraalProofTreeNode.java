package ch.heiafr.prolograal.nodes;

import ch.heiafr.prolograal.ProloGraalLanguage;
import ch.heiafr.prolograal.builtins.predicates.ProloGraalBuiltinClause;
import ch.heiafr.prolograal.exceptions.ProloGraalExistenceError;
import ch.heiafr.prolograal.runtime.*;
import com.oracle.truffle.api.CompilerDirectives.TruffleBoundary;
import com.oracle.truffle.api.nodes.Node;
import com.oracle.truffle.api.nodes.NodeInfo;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Class representing a node of the proof tree.
 * Each node is characterized by its goals.
 * This class is a Truffle executable {@link Node}.
 * @see ProloGraalResolverNode
 * @author Martin Spoto
 */
@NodeInfo(shortName = "ProofTreeNode")
public class ProloGraalProofTreeNode extends Node {

   private final Map<ProloGraalTerm<?>, List<ProloGraalClause>> clauses; // reference to the context clauses
   private final Deque<ProloGraalTerm<?>> goals; // the goals of this node

   public ProloGraalProofTreeNode(Map<ProloGraalTerm<?>, List<ProloGraalClause>> clauses,
                                  Deque<ProloGraalTerm<?>> goals) {
      this.clauses = clauses;
      this.goals = goals;
   }

   /**
    * Execute this node, creating a new child node containing the current goal list with its first goal replaced by
    * its body.
    * @param branches in-out parameter containing a list of branches.
    *                 Used to determine the starting point of this node when descending,
    *                 and to store the path to next possible branch when climbing back up the call stack after
    *                 a success is found.
    * @throws ProloGraalExistenceError if the context contains no clauses compatible with the first goal
    * @return either {@link ProloGraalSuccess} or {@link ProloGraalFailure}, depending on whether this proof tree
    * branch ends in a success (empty node) or not
    */
   @TruffleBoundary
   public ProloGraalBoolean execute(Deque<Integer> branches) throws ProloGraalExistenceError {

      if (ProloGraalLanguage.DEBUG_MODE) {
         System.out.println("ProofTreeNode : " + goals);
      }

      if (goals.isEmpty()) { // leaf node
         return new ProloGraalSuccess();
      }

      ProloGraalTerm<?> currentGoal = goals.peek();
      // get the list of all possible clauses based on the name of the predicate
      List<ProloGraalClause> possibleClauses = clauses.get(currentGoal);
      if (possibleClauses == null || possibleClauses.isEmpty()) // if no match, throw an error
      {
         throw new ProloGraalExistenceError(currentGoal);
      }

      int start = 0;
      if (!branches.isEmpty()) {
         // if we're redoing we need to skip to the right branch directly
         start = branches.pop();
      }

      // filter clauses that are unifiable with the current goal, creating copies and saving variables state as needed
      List<ProloGraalClause> unifiableClauses =
            IntStream.range(0, possibleClauses.size())
                  .filter(x -> {
                     ProloGraalClause clause = possibleClauses.get(x).copy();
                     currentGoal.save();
                     boolean r = clause.getHead().unify(currentGoal);
                     currentGoal.undo();
                     return r;
                  })
                  .mapToObj(x -> possibleClauses.get(x).copy()) // create a copy of each filtered clauses
                  .collect(Collectors.toList());

      for (int i = start; i < unifiableClauses.size(); i++) {
         // no need to copy here since it is already one
         ProloGraalClause unifiableClause = unifiableClauses.get(i);

         currentGoal.save();

         // unify the head with the current goal
         unifiableClause.getHead().unify(currentGoal);
         if (ProloGraalLanguage.DEBUG_MODE) {
            System.out.println("Unified " + currentGoal + " with " + unifiableClause.getHead());
         }

         // only execute built-in the first time we traverse their nodes
         if (branches.isEmpty()) {
            if (unifiableClause instanceof ProloGraalBuiltinClause) {
               // if the clause is a built-in, execute its internal behaviour
               ((ProloGraalBuiltinClause) unifiableClause).execute();
            }
         }

         // create a copy of the current goals
         Deque<ProloGraalTerm<?>> newGoals = new ArrayDeque<>(goals);

         List<ProloGraalTerm<?>> body = unifiableClause.getGoals();

         // always remove the first goal since it will be replaced
         newGoals.poll();

         // no need for distinction between facts and regular clauses
         // add all the new goals
         Collections.reverse(body);
         body.forEach(newGoals::addFirst);

         if (new ProloGraalProofTreeNode(clauses, newGoals).execute(branches).asBoolean()) {
            // skip last possible nodes if empty, or always add if we already have added a path
            if (!branches.isEmpty() || i + 1 < unifiableClauses.size()) {
               if (branches.isEmpty()) {
                  i = i + 1; // if we're at the bottom go directly to next node
               }
               branches.push(i); // add the path that gave the success
            }
            return new ProloGraalSuccess();
         }
         // undo all changes that the unification may have done
         currentGoal.undo();
      }

      return new ProloGraalFailure();
   }
}
