package ch.heiafr.prolograal.runtime;

import com.oracle.truffle.api.interop.TruffleObject;

/**
 * Abstract class representing a boolean in Prolog : either a success or a fail.
 * @see ProloGraalSuccess
 * @see ProloGraalFailure
 * @author Martin Spoto
 */
public abstract class ProloGraalBoolean implements TruffleObject {
   public abstract boolean asBoolean();
}