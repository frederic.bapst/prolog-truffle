package ch.heiafr.prolograal.builtins.predicates;

import ch.heiafr.prolograal.runtime.*;

import java.io.PrintWriter;

/**
 * Class representing the write/1(X) predicate.
 * Writes the string representation of X.
 * @author Martin Spoto
 * @see ProloGraalBuiltinClause
 */
public final class ProloGraalWriteBuiltin extends ProloGraalBuiltinClause {

   private final PrintWriter writer; // used for outputting
   private final ProloGraalContext context; // we keep the context for the copy method
   private ProloGraalVariable arg; // the variable X in write(X). We keep it to print it after the unification process

   public ProloGraalWriteBuiltin(ProloGraalContext context) {
      super();
      // get printer from context
      this.writer = new PrintWriter(context.getOutput(), true);
      this.context = context;

      // create the head of this clause
      // since we do not need custom unification, a simple structure is enough
      ProloGraalStructure head = new ProloGraalStructure(getVariables());
      head.setFunctor(new ProloGraalAtom(getVariables(), "write"));
      // we create and store the variable to access it more easily later in the execute method
      arg = new ProloGraalVariable(getVariables(), "_");
      head.addSubterm(arg);
      setHead(head);
   }

   /**
    * Execute the write, printing the string representation of whatever the variable is currently bound to.
    */
   @Override
   public void execute() {
      String str = arg.getRootValue().toString();
      if(str.startsWith("'") && str.endsWith("'")) {
         // strip single quotes
         str = str.substring(1, str.length()-1);
      }
      writer.print(str);
      writer.flush();
   }

   // override the default copy so we do not lose the built-in status
   @Override
   public ProloGraalClause copy() {
      return new ProloGraalWriteBuiltin(context);
   }

}
