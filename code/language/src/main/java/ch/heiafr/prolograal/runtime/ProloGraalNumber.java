package ch.heiafr.prolograal.runtime;

import java.util.Map;

/**
 * Abstract class to represent Prolog numbers.
 * @param <T> Meant to be the type of the subclass itself,
 *            so that it can properly implement the {@link ProloGraalTerm#copy(Map)} method.
 * @see ProloGraalIntegerNumber
 * @see ProloGraalDoubleNumber
 * @author Martin Spoto
 */
public abstract class ProloGraalNumber<T extends ProloGraalTerm<T>> extends ProloGraalTerm<T> {
   protected ProloGraalNumber(Map<ProloGraalVariable, ProloGraalVariable> variables) {
      super(variables);
   }

   public abstract double asDouble();

   public abstract int asInt();

   @Override
   public boolean equals(Object obj) {
      return obj instanceof ProloGraalNumber && ((ProloGraalNumber<?>) obj).asDouble() == this.asDouble() && ((ProloGraalNumber<?>) obj).asInt() == this.asInt();
   }

   /**
    * Unify this number with another.
    * Assumes that the equals method is correctly redefined in child classes.
    */
   @Override
   public boolean unify(ProloGraalTerm<?> other) {
      if (other instanceof ProloGraalVariable) {
         return other.unify(this);
      } else {
         return this.equals(other);
      }
   }
}