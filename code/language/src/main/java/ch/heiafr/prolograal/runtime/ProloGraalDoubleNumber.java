package ch.heiafr.prolograal.runtime;

import java.util.Map;

/**
 * Class representing a Prolog floating point number. Unification is handled in the super class.
 * @author Martin Spoto
 */
public final class ProloGraalDoubleNumber extends ProloGraalNumber<ProloGraalDoubleNumber> {
   private final double value;

   public ProloGraalDoubleNumber(Map<ProloGraalVariable, ProloGraalVariable> variables, double value) {
      super(variables);
      this.value = value;
      this.hashCode = Double.hashCode(value);
   }

   @Override
   public double asDouble() {
      return value;
   }

   @Override
   public int asInt() {
      return (int) value;
   }

   @Override
   public String toString() {
      return value + "";
   }

   @Override
   public boolean equals(Object obj) {
      return obj instanceof ProloGraalDoubleNumber && this.value == ((ProloGraalDoubleNumber) obj).value;
   }

   @Override
   public ProloGraalDoubleNumber copy(Map<ProloGraalVariable, ProloGraalVariable> variables) {
      return new ProloGraalDoubleNumber(variables, this.value);
   }
}