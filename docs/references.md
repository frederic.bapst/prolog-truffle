# Références
Ce document contient les différentes références et ressources utilisées pour le projet. Il sert principalement de liste en attendant une version plus formelle dans le rapport final.
## SimpleLanguage
Projet d'exemple commenté utilisant un langage fictif et Truffle, disponible à cette adresse : https://github.com/graalvm/simplelanguage  
Ce projet montre l'utilisation de Truffle et de GraalVM via un langage de programmation conçu pour l'occasion, comprenant de nombreux concepts permettant de démontrer les capacités de Truffle.
## One VM to Rule Them All, One VM to Bind Them
Conférence Oracle sur Truffle et GraalVM, disponible à cette adresse : https://youtu.be/FJY96_6Y3a4  
Les slides de la présentation sont disponible à cette adresse : https://lafo.ssw.uni-linz.ac.at/pub/papers/2016_PLDI_Truffle.pdf  
Cette conférence utilise le langage d'exemple SimpleLanguage afin de démontrer certains concepts d'utilisation de Truffle et de GraalVM, notamment sur le fonctionement de l'AST (Abstract Syntax Tree) et des spécialisations.  
De nombreux conseils d'optimisation et de bonnes pratiques sont également présentés.
## Warren's Abstract Machine: A Tutorial Reconstruction
Aït-Kaci H., _Warren's Abstract Machine: A Tutorial Reconstruction_, Simon Fraser University, MIT Press, 1991