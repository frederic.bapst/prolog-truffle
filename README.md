# Prolog avec Truffle et GraalVM

GraalVM (connu en tant que "JVM ultra-rapide") et Truffle sont des technologies récentes permettant d'implémenter des langages de programmation à partir de la définition d'un interpréteur, avec une approche très intéressante aussi bien du côté pratique (optimisations automatiques) que théorique (Futamura projections). Plusieurs langages (R, Python, Ruby, JavaScript) ont déjà été portés avec succès, et la démarche générale pour créer un nouveau langage est documentée (avec l'exemple-tutoriel SimpleLangage).

Dans ce projet, il s'agit de découvrir le fonctionnement de Truffle en essayant d'y porter partiellement une version simplifiée du langage Prolog (cela semble n'avoir pas encore été envisagé). Le fait que le cours "Programmation logique" ait lieu en parallèle n'est pas du tout gênant : c'est la technologie Truffle qui sera étudiée, et nous nous concentrerons sur quelques mécanismes élémentaires de Prolog.

## Structure du projet

Le projet est découpé en deux sections :
 - /docs : contient la documentation du projet, avec les éléments suivants :
   - /report/report.pdf : rapport finale du projet
   - /manual/manual.pdf : manuel d'installation et d'utilisation
   - /specs/specs.pdf et /specs/planning.pdf : spécifications et planning du projet
   - /pv/... : PV des différentes réunions
 - /code : contient le code du projet

## Compilation et utilisation du projet

Se référer au manuel d'installation (/docs/manual/manual.pdf).